package com.sda.ee.mvc;

import com.sda.ee.mvc.dao.auction.AuctionDaoImpl;
import com.sda.ee.mvc.dao.auction.IAuctionDao;
import com.sda.ee.mvc.dao.comment.CommentDao;
import com.sda.ee.mvc.dao.comment.ICommentDao;
import com.sda.ee.mvc.dao.product.IProductDao;
import com.sda.ee.mvc.dao.product.ProductDao;
import com.sda.ee.mvc.dao.user.IUserDao;
import com.sda.ee.mvc.dao.user.UserDao;
import com.sda.ee.mvc.services.auction.AuctionServiceImp;
import com.sda.ee.mvc.services.auction.IAuctionService;
import com.sda.ee.mvc.services.comment.CommentServiceImpl;
import com.sda.ee.mvc.services.comment.ICommentService;
import com.sda.ee.mvc.services.product.IProductService;
import com.sda.ee.mvc.services.product.ProductServiceImpl;
import com.sda.ee.mvc.services.user.IUserService;
import com.sda.ee.mvc.services.user.UserServiceImpl;

public class BeanController {
    private static IUserService userServiceImpl = new UserServiceImpl();
    private static IUserDao userDaoImpl = new UserDao();
    private static IAuctionService auctionService = new AuctionServiceImp();
    private static IAuctionDao auctionDao = new AuctionDaoImpl();
    private static IProductService productService = new ProductServiceImpl();
    private static IProductDao productDao = new ProductDao();
    private static ICommentService commentService = new CommentServiceImpl();
    private static ICommentDao commentDao = new CommentDao();

    // getter user service
    public static IUserService getUserServiceImpl() {
        if (userServiceImpl == null) {
            userServiceImpl = new UserServiceImpl();
        }
        return userServiceImpl;
    }

    public static IAuctionService getAuctionServiceImpl() {
        if (auctionService == null) {
            auctionService = new AuctionServiceImp();
        }
        return auctionService;
    }

    public static IProductService getProductServiceImpl() {
        if (productService == null) {
            productService = new ProductServiceImpl();
        }
        return productService;
    }

    // getter user dao
    public static IUserDao getUserDaoImpl() {
        if (userDaoImpl == null) {
            userDaoImpl = new UserDao();
        }
        return userDaoImpl;
    }

    public static IAuctionDao getAuctionDao() {
        if (auctionDao == null) {
            auctionDao = new AuctionDaoImpl();
        }
        return auctionDao;
    }

    public static IProductDao getProductDao() {
        if (productDao == null) {
            productDao = new ProductDao();
        }
        return productDao;
    }

    public static ICommentService getCommentServiceImpl() {
        if (commentService == null) {
            commentService = new CommentServiceImpl();
        }
        return commentService;
    }

    public static ICommentDao getCommentDao() {
        if (commentDao == null) {
            commentDao = new CommentDao();
        }
        return commentDao;
    }
}
