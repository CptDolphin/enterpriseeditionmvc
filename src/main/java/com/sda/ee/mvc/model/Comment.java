package com.sda.ee.mvc.model;

public class Comment {
    private String comment;
    private int id;
    private int auctionid;
    private int commenterid;

    public Comment(String comment, int id, int auctionid, int commenterid) {
        this.comment = comment;
        this.id = id;
        this.auctionid = auctionid;
        this.commenterid = commenterid;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "comment='" + comment + '\'' +
                ", id=" + id +
                ", auctionid=" + auctionid +
                ", commenterid=" + commenterid +
                '}';
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAuctionid() {
        return auctionid;
    }

    public void setAuctionid(int auctionid) {
        this.auctionid = auctionid;
    }

    public int getCommenterid() {
        return commenterid;
    }

    public void setCommenterid(int commenterid) {
        this.commenterid = commenterid;
    }
}
