package com.sda.ee.mvc.dao.user;

import com.sda.ee.mvc.model.User;

import java.util.List;

public interface IUserDao {
    List<User> getAllUsers();

    boolean checkIfUserOrEmailExists(String newLogin, String newEmail);

    void addUser(User user);
}
