package com.sda.ee.mvc.dao.comment;

import com.sda.ee.mvc.model.Comment;

import java.util.List;

public interface ICommentDao {
    void addComment(Comment toAdd);
    List<Comment> getAllComments();
    Comment searchCommentByProductID(int productID);
    List<Comment> searchCommentsByUserID(int userID);
}
