package com.sda.ee.mvc.dao.auction;

import com.sda.ee.mvc.model.Auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuctionDaoImpl implements IAuctionDao {
    private Map<Integer,Auction> auctionList = new HashMap<>();
    @Override
    public void addAuction(Auction toAdd) {
        auctionList.put(toAdd.getId(),toAdd);
    }

    @Override
    public List<Auction> getAllAuctions() {
        return new ArrayList<>(auctionList.values());
    }
}
