package com.sda.ee.mvc.dao.product;

import com.sda.ee.mvc.model.Product;

import java.util.*;

public class ProductDao implements IProductDao{
    private Map<Integer,Product> productMap = new HashMap<>();
    @Override
    public Optional<Product> getProductWithId(int productId) {
        return Optional.ofNullable(productMap.get(productId));
    }

    @Override
    public void addProduct(Product toAdd) {
        productMap.put(toAdd.getId(),toAdd);
    }

    @Override
    public List<Product> getAllProducts() {
        return new ArrayList<>(productMap.values());
    }
}
