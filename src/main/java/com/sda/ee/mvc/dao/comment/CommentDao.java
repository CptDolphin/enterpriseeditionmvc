package com.sda.ee.mvc.dao.comment;


import com.sda.ee.mvc.model.Comment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommentDao implements ICommentDao {
    private Map<Integer, Comment> commentMap = new HashMap<>();

    @Override
    public void addComment(Comment toAdd) {
        commentMap.put(toAdd.getId(), toAdd);
    }

    @Override
    public List<Comment> getAllComments() {
        return new ArrayList<>(commentMap.values());
    }

    @Override
    public Comment searchCommentByProductID(int productID) {
        return commentMap.get(productID);
    }

    @Override
    public List<Comment> searchCommentsByUserID(int userID) {
        return commentMap.values().stream()
                .filter(comment -> comment.getCommenterid() == userID)
                .collect(Collectors.toList());
    }
}
