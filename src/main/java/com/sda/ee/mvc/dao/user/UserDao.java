package com.sda.ee.mvc.dao.user;

import com.sda.ee.mvc.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDao implements IUserDao {

    // data access object
    // obiekt który dostarcza danych nt. innych obiektów z baz danych itd.
    private List<User> list = new ArrayList<User>();

    public List<User> getAllUsers() {
        return list;
    }

    @Override
    public boolean checkIfUserOrEmailExists(String newLogin, String newEmail) {
        Optional<User> userOptional = list.stream()
                .filter(user -> user.getLogin().equalsIgnoreCase(newLogin))
                .filter(user -> user.getEmail().equalsIgnoreCase(newEmail))
                .findAny();
        //jesli taki user nie istnieje to zwracamy true!
        //sukcese jest wtedy, kiedy taki uzytkownik nie istniejeN
        return !userOptional.isPresent();
    }

    @Override
    public void addUser(User user) {
        list.add(user);
    }
    public User getUser(int userID){
        return list.get(userID);
    }
}
