package com.sda.ee.mvc.services.user;

import com.sda.ee.mvc.model.User;

import java.util.Optional;

public interface IUserService {

    Optional<User> login(String login, String passwordHash);

    boolean validateUser(int userId, String token);

    boolean register(String newLogin, String newPassword, String newEmail);
}
