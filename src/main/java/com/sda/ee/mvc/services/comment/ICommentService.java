package com.sda.ee.mvc.services.comment;

import com.sda.ee.mvc.exceptions.CommenterNotFoundException;
import com.sda.ee.mvc.model.Comment;

import java.util.List;

public interface ICommentService {
    boolean addComment(String tekst, int productID, int commenterID);
    List<Comment> getAllComments();
    Comment searchCommentByProductID(int productID) throws CommenterNotFoundException;
    List<Comment> searchCommentsByUserID(int userID);
}
