package com.sda.ee.mvc.services.auction;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.dao.auction.IAuctionDao;
import com.sda.ee.mvc.model.Auction;
import com.sda.ee.mvc.model.Product;

import java.util.List;

public class AuctionServiceImp implements IAuctionService {
    private static int NEXT_AUCTION_ID = 0;
    private IAuctionDao auctionDao = BeanController.getAuctionDao();
    @Override
    public boolean addAuction(int userId, String title, double price, int amount, Product product) {
        Auction toAdd = new Auction(NEXT_AUCTION_ID++,title,price,product,amount);
        auctionDao.addAuction(toAdd);
        return false;
    }

    @Override
    public List<Auction> getAllAuctions() {
        return auctionDao.getAllAuctions();
    }
}
