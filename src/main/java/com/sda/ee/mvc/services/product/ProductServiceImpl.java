package com.sda.ee.mvc.services.product;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.dao.product.IProductDao;
import com.sda.ee.mvc.exceptions.ItemNotFoundException;
import com.sda.ee.mvc.model.Product;
import com.sda.ee.mvc.model.State;

import java.util.List;
import java.util.Optional;

public class ProductServiceImpl implements IProductService {
    private IProductDao productDao = BeanController.getProductDao();
    private static int NEXT_PRODUCT_ID = 0;
    @Override
    public Product getProductWithId(int productId) throws ItemNotFoundException{
        Optional<Product> productOptional = productDao.getProductWithId(productId);
        if (productOptional.isPresent()){
            return productOptional.get();
        }else{
            throw new ItemNotFoundException();
        }
    }

    @Override
    public boolean addProduct(String name, State state) {
        Product toAdd = new Product(NEXT_PRODUCT_ID++,name,state);
        productDao.addProduct(toAdd);
        return true;
    }

    @Override
    public List<Product> getAllProducts() {
        return productDao.getAllProducts();
    }
}
