package com.sda.ee.mvc.services.user;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.dao.user.IUserDao;
import com.sda.ee.mvc.model.User;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements IUserService {
    private static int USER_ID_COUNTER = 0;
    private IUserDao userDao = BeanController.getUserDaoImpl();

    public Optional<User> login(String login, String passwordHash) {
        List<User> allUser = userDao.getAllUsers();
        for (User user : allUser) {
            if(user.getLogin().equalsIgnoreCase(login) &&
                    user.getPassword().equals(passwordHash)){
                return Optional.ofNullable(user);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean validateUser(int userId, String token) {
        return false;
    }

    @Override
    public boolean register(String newLogin, String newPassword, String newEmail) {
        //check user with that login and/or email
        if(userDao.checkIfUserOrEmailExists(newLogin,newEmail)){
            //jesli udalo nam sie wjejsc w if- sukces - taki uzytlownilk nie istnieje
            User user = new User(USER_ID_COUNTER++,newLogin,newEmail,newPassword);
            userDao.addUser(user);

            return true;
        }
        return false;
    }
}
