package com.sda.ee.mvc.services.comment;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.dao.comment.ICommentDao;
import com.sda.ee.mvc.exceptions.CommenterNotFoundException;
import com.sda.ee.mvc.model.Comment;

import java.util.List;
import java.util.Optional;

public class CommentServiceImpl implements ICommentService {
    private static int NEXT_COMMENT_ID = 0;
    private ICommentDao commentDao= BeanController.getCommentDao();


    @Override
    public boolean addComment(String tekst, int productID, int commenterID) {
        Comment toAdd = new Comment(tekst, NEXT_COMMENT_ID++, productID,commenterID);
        commentDao.addComment(toAdd);
        return true;
    }

        @Override
    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    @Override
    public Comment searchCommentByProductID(int productID) throws CommenterNotFoundException {
        Optional<Comment> commentOptional = Optional.ofNullable(commentDao.searchCommentByProductID(productID));
        if(commentOptional.isPresent()){
            return commentOptional.get();
        }else{
            throw new CommenterNotFoundException();
        }
    }

    @Override
    public List<Comment> searchCommentsByUserID(int userID) {
        return commentDao.searchCommentsByUserID(userID);
    }
}
