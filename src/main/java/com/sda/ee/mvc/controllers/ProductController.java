package com.sda.ee.mvc.controllers;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.model.Product;
import com.sda.ee.mvc.model.State;
import com.sda.ee.mvc.services.product.IProductService;

import java.util.List;

public class ProductController {
    private IProductService productService = BeanController.getProductServiceImpl();
    public void addProduct(String name, State state){
        if(productService.addProduct(name,state)){
            System.out.println("Product added");
        }else{
            System.out.println("Error adding product");
        }
    }
    public void listAllProducts(){
        List<Product> products = productService.getAllProducts();
        products.forEach(System.out::println);
    }
}
