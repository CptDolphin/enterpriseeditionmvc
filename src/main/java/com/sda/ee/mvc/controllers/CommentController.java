package com.sda.ee.mvc.controllers;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.exceptions.CommenterNotFoundException;
import com.sda.ee.mvc.model.Comment;
import com.sda.ee.mvc.services.comment.ICommentService;
import com.sda.ee.mvc.services.user.IUserService;

import java.util.List;

public class CommentController {
    private ICommentService commentService = BeanController.getCommentServiceImpl();
    private IUserService userService = BeanController.getUserServiceImpl();

    public void addComent(String text, int productID, int commenterID) {
        commentService.addComment(text, productID, commenterID);
        System.out.println("Successfully added comment");
    }

    public void listAllComments() {
        List<Comment> comments = commentService.getAllComments();
        comments.forEach(System.out::println);
    }

    public void searchCommentByProductID(int productID) {
        Comment comment = null;
        try {
            comment = commentService.searchCommentByProductID(productID);
        } catch (CommenterNotFoundException e) {
            System.out.println("No comment to product with such ID");
            System.out.println(e.getMessage());
        }
        System.out.println(comment.toString());
        System.out.println("Succesfuly found product comment");
    }

    public void searchCommentsByUserID(int userID) {
        List<Comment> comments = commentService.searchCommentsByUserID(userID);
        System.out.println(comments.toString());
        System.out.println("Succesfuly given user comments");

    }
}
