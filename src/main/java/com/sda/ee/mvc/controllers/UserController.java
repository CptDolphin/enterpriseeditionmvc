package com.sda.ee.mvc.controllers;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.model.User;
import com.sda.ee.mvc.services.user.IUserService;

import java.util.Optional;

public class UserController {

    private IUserService userService = BeanController.getUserServiceImpl();

    public Integer loginUser(String login, String passwordHash) {
        Optional<User> loggedInUser = userService.login(login, passwordHash);
        if (loggedInUser.isPresent()) {
            System.out.println("Successful login");
            return loggedInUser.get().getId();
        } else {
            System.out.println("Unsuccessful login!");
            return null;
        }
    }

    public void registerUser(String newLogin, String newPassword, String newEmail) {
        boolean succesfulRegister = userService.register(newLogin,newPassword,newEmail);
        if(succesfulRegister){
            System.out.println("Successfull Register!");
        }else{
            System.out.println("Unsuccessful Register!");
        }
    }
}
