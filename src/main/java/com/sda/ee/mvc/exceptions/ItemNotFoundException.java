package com.sda.ee.mvc.exceptions;

public class ItemNotFoundException extends Exception{
    public ItemNotFoundException() {
        super("Item not found in database!");
    }
}
