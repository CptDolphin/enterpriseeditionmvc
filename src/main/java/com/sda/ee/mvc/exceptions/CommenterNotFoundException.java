package com.sda.ee.mvc.exceptions;

public class CommenterNotFoundException extends Exception {
    public CommenterNotFoundException() {
        super("There is no commenter with such id");
    }
}
