package com.sda;

import com.sda.ee.mvc.controllers.AuctionController;
import com.sda.ee.mvc.controllers.CommentController;
import com.sda.ee.mvc.controllers.ProductController;
import com.sda.ee.mvc.controllers.UserController;
import com.sda.ee.mvc.model.State;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        UserController userController = new UserController();
        ProductController productController = new ProductController();
        AuctionController auctionController = new AuctionController();
        CommentController commentController = new CommentController();

        Integer currentUserId = null;

        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine();
            String[] splits = line.split(" ");

            if (line.startsWith("register")) {
                registering(userController, splits);
            } else if (line.startsWith("login")) {

                if (currentUserId == null) {
                String newLogin = splits[1];
                String newPassword = splits[2];
                    //niech logowanie bedzie dostepne tylko jak jestesmy wylogowani
                    String hash = MD5(newPassword);
                    currentUserId = userController.loginUser(newLogin, hash);
                } else {
                    System.out.println("Unable to login, log out first");
                }
            } else if (line.equalsIgnoreCase("logout")) {
                if (currentUserId == null) {
                    System.out.println("Already logged out");
                } else {
                    currentUserId = null;
                }
            } else if (line.startsWith("addproduct")) {
                if (currentUserId == null) {
                    System.out.println("You must first login to add product");
                } else {
                    String[] split = line.split(" ");
                    String name = splits[1];
                    String state = splits[2];
                    productController.addProduct(name, State.valueOf(state.toUpperCase()));
                }
            } else if (line.startsWith("listproducts")) {
                productController.listAllProducts();
            } else if (line.startsWith("addauction")) {
                String title = splits[1];
                double price = Double.parseDouble(splits[2]);
                int amount = Integer.parseInt(splits[3]);
                int productId = Integer.parseInt(splits[4]);
                auctionController.addAuction(currentUserId, title, price, amount, productId);
            } else if (line.startsWith("listauctions")) {
                auctionController.listAllAuction();
            } else if (line.startsWith("addcomment")) {
                String comment = splits[1];
                int productID = Integer.parseInt(splits[2]);
                int comenterID = Integer.parseInt(splits[3]);
                commentController.addComent(comment,productID,comenterID);
            } else if (line.startsWith("listcomments")) {
                commentController.listAllComments();
            } else if (line.startsWith("searchcommentbyproductid")){
                int searchedComment = Integer.parseInt(splits[1]);
                commentController.searchCommentByProductID(searchedComment);
            } else if(line.startsWith("searchcommentbyuserid")){
                int userID =  Integer.parseInt(splits[1]);
                commentController.searchCommentsByUserID(userID);
            }
        }
    }

    private static void registering(UserController userController, String[] splits) {
        String newLogin = splits[1];
        String newPassword = splits[2];

        String hash = MD5(newPassword);
        System.out.println(hash);

        String newEmail = splits[3];
        userController.registerUser(newLogin, hash, newEmail);
    }

    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
}
