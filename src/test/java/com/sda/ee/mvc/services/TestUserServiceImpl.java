package com.sda.ee.mvc.services;

import com.sda.ee.mvc.model.User;
import com.sda.ee.mvc.services.user.IUserService;

import java.util.Optional;

public class TestUserServiceImpl implements IUserService {

    @Override
    public Optional<User> login(String login, String passwordHash) {
        return Optional.empty();
    }

    @Override
    public boolean validateUser(int userId, String token) {
        return false;
    }

    @Override
    public boolean register(String newLogin, String newPassword, String newEmail) {
        return false;
    }
}
